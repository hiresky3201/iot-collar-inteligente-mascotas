package me.iot.handlers;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import dev.morphia.query.experimental.filters.Filters;
import io.undertow.server.HttpServerExchange;
import me.iot.entities.User;
import me.iot.exceptions.InvalidUserOrPasswordException;
import me.iot.exceptions.UnauthorizedException;
import me.iot.persistence.Persistence;
import me.iot.udis.annotations.FromJsonBody;
import me.iot.udis.annotations.Route;
import me.iot.udis.core.HandlersParameterArray;
import me.iot.udis.enums.HttpMethod;
import me.iot.udis.wrappers.ServerExchange;
import me.iot.utils.StringUtils;

import java.io.UnsupportedEncodingException;
import java.util.Optional;

public class AuthorizationHandlers {
    public static Algorithm JWT_ALGORITHM;
    public static JWTVerifier JWT_VERIFIER;
    public static final String ISSUER = "iot-mascotas";
    public static final String BEARER_PREFIX = "Bearer ";
    public static final String AUTHORIZATION_HEADER = "Authorization";

    static {
        // -- Build JWT tools
        try {
            JWT_ALGORITHM = Algorithm.HMAC256( "MY_SECRET_KEY" );
            JWT_VERIFIER = JWT.require( JWT_ALGORITHM ).withIssuer( ISSUER ).build();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns current user in request
     * @param exchange
     * @param user
     */
    @Route( routes = "/authorization/current-user", methods = HttpMethod.GET )
    public static void fetchUser(ServerExchange exchange, Persistence persistence, User user){
        // -- Return data
        exchange.apiResponse( user );
    }

    /**
     * Generates an encoded JWT token and returns it
     * The token can be generated for an user, which must be specified in the body
     * with "tokenType"
     *
     * The value of this must be user or another type
     *
     * @param serverExchange
     * @param auth
     */
    @Route( routes = "/authorization/get-token" , methods = HttpMethod.POST )
    public static void getToken(ServerExchange serverExchange , Persistence persistence, @FromJsonBody Auth auth ){
        String encodedToken = buildEncodedTokenForUser( persistence, auth.getUsername() , auth.getPassword() );

        serverExchange.setCookie( "jwtToken", encodedToken );
        serverExchange.apiResponse( encodedToken );
    }

    /**
     * Verifies user by trying to decode JWT token if request has any
     * If found, returns user associated with the token ID
     *
     * @param exchange
     * @param parameterArray
     */
    public static User verifyUser(HttpServerExchange exchange , HandlersParameterArray parameterArray){
        DecodedJWT token = parameterArray.get( DecodedJWT.class );
        if( token == null ) token = decodeToken( exchange , parameterArray );
        if( token == null ) throw new UnauthorizedException();

        // -- We need to ensure every handler has the Persistence parameter before User
        Persistence persistence = parameterArray.get( Persistence.class );

        return Optional.of( persistence.getEntityById( User.class, token.getClaim("user").asString() ) ).orElseThrow( InvalidUserOrPasswordException::new );
    }

    /**
     * Decode a JWT token by searching for token
     * If no token is found, null is returned
     * Otherwise, a DecodedJWT is returned
     *
     * @param exchange
     * @param parameterArray
     * @return
     */
    public static DecodedJWT decodeToken(HttpServerExchange exchange , HandlersParameterArray parameterArray ){
        String encodedToken = null;

        // -- Get token from cookie
        if( exchange.getRequestCookies().containsKey( "jwtToken" ) ){
            encodedToken = exchange.getRequestCookies().get("jwtToken").getValue();
        }
        // -- Get token from header
        if( exchange.getRequestHeaders().contains( AUTHORIZATION_HEADER ) ){
            encodedToken = exchange.getRequestHeaders().get( AUTHORIZATION_HEADER ).getFirst().replace( BEARER_PREFIX , StringUtils.EMPTY );
        }

        // -- If not token found by this point, return
        if( encodedToken == null ) return null;

        return JWT_VERIFIER.verify( encodedToken );
    }

    private static String buildEncodedTokenForUser(Persistence persistence, String username, String password){
        User user = persistence.executeQuery( User.class, Filters.eq("email", username ) ).stream().findFirst().orElse( null );

        if( user == null || !user.checkPassword( password ) ) throw new InvalidUserOrPasswordException();

        JWTCreator.Builder builder = JWT.create().withIssuer( ISSUER );
        builder.withClaim( "user" , user.getId() );

        return builder.sign( JWT_ALGORITHM );
    }
}

class Auth{
    private String username;
    private String password;

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }
}