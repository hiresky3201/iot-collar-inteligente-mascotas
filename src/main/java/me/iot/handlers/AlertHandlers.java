package me.iot.handlers;

import dev.morphia.query.experimental.filters.Filters;
import me.iot.entities.Alert;
import me.iot.entities.Log;
import me.iot.entities.User;
import me.iot.persistence.Persistence;
import me.iot.udis.annotations.FromJsonBody;
import me.iot.udis.annotations.FromQuery;
import me.iot.udis.annotations.Route;
import me.iot.udis.enums.HttpMethod;
import me.iot.udis.wrappers.ServerExchange;

public class AlertHandlers {
    /**
     * Creates a new alert
     *
     * @param exchange
     * @param persistence
     */
    @Route( routes = "/alerts/create", methods = HttpMethod.POST )
    public static void createAlert(ServerExchange exchange, Persistence persistence, User user, @FromJsonBody CreateAlert createAlert){
        Alert alert = new Alert();
        alert.setUserId( user.getId() );
        alert.setActive( true );
        alert.setThreshold( createAlert.getThreshold() );
        alert.setSensorId( createAlert.getSensorId() );
        alert.setMethod( createAlert.getMethod() );
        alert.setType( createAlert.getType() );
        alert.setMinutesBetweenAlertNotifications( createAlert.getMinutesBetweenAlertNotifications() );

        persistence.createEntity( alert );
        exchange.apiResponse( alert );
    }

    /**
     * Modifies an existing alert
     * @param exchange
     * @param persistence
     * @param user
     * @param expandAlert
     */
    @Route( routes = "/alerts/expand", methods = HttpMethod.POST )
    public static void expandAlert(ServerExchange exchange, Persistence persistence, User user, @FromJsonBody ExpandAlert expandAlert){
        Alert alert = persistence.getEntityById( Alert.class, expandAlert.getAlertId() );

        // -- Modify alert request
        if( expandAlert.getSensorId() != null ){
            alert.setType( expandAlert.getType() );
            alert.setMethod( expandAlert.getMethod() );
            alert.setSensorId( expandAlert.getSensorId() );
            alert.setMinutesBetweenAlertNotifications( expandAlert.getMinutesBetweenAlertNotifications() );
        }
        // -- Alert toggle request
        else{
            alert.setActive( expandAlert.isActive() );
        }

        persistence.merge( alert );
        exchange.apiResponse( "OK" );
    }

    /**
     * Fetches all logs from the user
     *
     * @param exchange
     * @param persistence
     * @param user
     */
    @Route( routes = "/logs", methods = HttpMethod.GET )
    public static void fetchLogs(ServerExchange exchange, Persistence persistence, User user){
        exchange.apiResponse( persistence.executeQuery( Log.class, Filters.eq( "userId", user.getId() ) ) );
    }

    /**
     * Returns the list of all alerts an user has
     * @param exchange
     * @param persistence
     * @param user
     */
    @Route( routes = "/alerts", methods = HttpMethod.GET )
    public static void fetchAlerts(ServerExchange exchange, Persistence persistence, User user){
        exchange.apiResponse( persistence.executeQuery( Alert.class, Filters.eq( "userId", user.getId() ) ) );
    }

    /**
     * Deletes an alert from the user
     * @param exchange
     * @param persistence
     * @param user
     * @param alertId
     */
    @Route( routes = "/alerts/delete", methods = HttpMethod.POST)
    public static void deleteAlert(ServerExchange exchange, Persistence persistence, User user, @FromQuery( "id" ) String alertId){
        Alert alert = persistence.getEntityById( Alert.class, alertId );
        persistence.delete( alert );

        exchange.apiResponse( "OK" );
    }
}

class CreateAlert{
    private Alert.Type type;
    private Alert.Method method;
    private String sensorId;
    private double threshold;
    private int minutesBetweenAlertNotifications;

    public String getSensorId() { return sensorId; }

    public void setSensorId(String sensorId) { this.sensorId = sensorId; }

    public Alert.Type getType() { return type; }

    public void setType(Alert.Type type) { this.type = type; }

    public Alert.Method getMethod() { return method; }

    public void setMethod(Alert.Method method) { this.method = method; }

    public double getThreshold() { return threshold; }

    public void setThreshold(double threshold) { this.threshold = threshold; }

    public int getMinutesBetweenAlertNotifications() { return minutesBetweenAlertNotifications; }

    public void setMinutesBetweenAlertNotifications(int minutesBetweenAlertNotifications) { this.minutesBetweenAlertNotifications = minutesBetweenAlertNotifications; }
}

class ExpandAlert{
    private String alertId;
    private Alert.Type type;
    private Alert.Method method;
    private String sensorId;
    private boolean active;
    private int minutesBetweenAlertNotifications;

    public boolean isActive() { return active; }

    public void setActive(boolean active) { this.active = active; }

    public String getAlertId() { return alertId; }

    public void setAlertId(String alertId) { this.alertId = alertId; }

    public String getSensorId() { return sensorId; }

    public void setSensorId(String sensorId) { this.sensorId = sensorId; }

    public Alert.Type getType() { return type; }

    public void setType(Alert.Type type) { this.type = type; }

    public Alert.Method getMethod() { return method; }

    public void setMethod(Alert.Method method) { this.method = method; }

    public int getMinutesBetweenAlertNotifications() { return minutesBetweenAlertNotifications; }

    public void setMinutesBetweenAlertNotifications(int minutesBetweenAlertNotifications) { this.minutesBetweenAlertNotifications = minutesBetweenAlertNotifications; }
}
