package me.iot.handlers;

import dev.morphia.query.experimental.filters.Filters;
import me.iot.entities.Area;
import me.iot.entities.Pet;
import me.iot.entities.User;
import me.iot.exceptions.ExistingSensorIdException;
import me.iot.persistence.Persistence;
import me.iot.udis.annotations.FromJsonBody;
import me.iot.udis.annotations.Route;
import me.iot.udis.enums.HttpMethod;
import me.iot.udis.wrappers.ServerExchange;
import org.mindrot.jbcrypt.BCrypt;

public class AppHandlers {
    /**
     * Creates a new user
     *
     * @param exchange
     * @param persistence
     * @param registerUser
     */
    @Route( routes = "/users/create", methods = HttpMethod.POST )
    public static void createUser(ServerExchange exchange, Persistence persistence, @FromJsonBody RegisterUser registerUser) {
        User user = new User();
        user.setName( registerUser.getName() );
        user.setEmail( registerUser.getEmail() );
        user.setPassword( BCrypt.hashpw( registerUser.getPassword() , BCrypt.gensalt( 12 ) ) );

        persistence.createEntity( user );
        exchange.apiResponse( user );
    }

    /**
     * Creates a new pet
     *
     * @param exchange
     * @param persistence
     * @param user
     * @param createPet
     */
    @Route( routes = "/pets/create", methods = HttpMethod.POST)
    public static void createPet(ServerExchange exchange, Persistence persistence, User user, @FromJsonBody CreatePet createPet) throws Exception {
        Pet existingPet = persistence.executeQuery( Pet.class, Filters.eq( "sensorId", createPet.getSensorId() ) ).stream().findFirst().orElse( null );

        if( existingPet != null ) throw new ExistingSensorIdException();

        Pet pet = new Pet();
        pet.setName( createPet.getName() );
        pet.setAreaId( createPet.getAreaId() );
        pet.setSensorId( createPet.getSensorId() );
        pet.setUserId( user.getId() );

        persistence.createEntity( pet );
        exchange.apiResponse( pet );
    }

    /**
     * Creates a new area
     * @param exchange
     * @param persistence
     * @param user
     * @param createArea
     */
    @Route( routes = "/areas/create", methods = HttpMethod.POST)
    public static void createArea(ServerExchange exchange, Persistence persistence, User user, @FromJsonBody CreateArea createArea){
        Area area = new Area();
        area.setUserId( user.getId() );
        area.setLng( createArea.getLng() );
        area.setLat( createArea.getLat() );
        area.setName( createArea.getName() );
        area.setMidLevelMeters( createArea.getMidLevelMeters() );
        area.setMaxLevelMeters( createArea.getMaxLevelMeters() );

        persistence.createEntity( area );
        exchange.apiResponse( area );
    }

    /**
     * Adds a pet to an existing area
     *
     * @param exchange
     * @param persistence
     * @param user
     * @param addPetToArea
     */
    @Route( routes = "/pets/area/add", methods = HttpMethod.POST)
    public static void addPetToArea(ServerExchange exchange, Persistence persistence, User user, @FromJsonBody AddPetToArea addPetToArea){
        Pet pet = persistence.getEntityById( Pet.class, addPetToArea.getPetId() );
        Area area = persistence.getEntityById( Area.class, addPetToArea.getAreaId() );

        // -- Retrieving area to verify it exists
        pet.setAreaId( area.getId() );
        exchange.apiResponse( "OK" );
    }

    /**
     * Returns the list of all pets an user has
     * @param exchange
     * @param persistence
     * @param user
     */
    @Route( routes = "/areas", methods = HttpMethod.GET )
    public static void fetchAreas(ServerExchange exchange, Persistence persistence, User user){
        exchange.apiResponse( persistence.executeQuery( Area.class, Filters.eq( "userId", user.getId() ) ) );
    }

    /**
     * Returns the list of all pets an user has
     * @param exchange
     * @param persistence
     * @param user
     */
    @Route( routes = "/pets", methods = HttpMethod.GET )
    public static void fetchPets(ServerExchange exchange, Persistence persistence, User user){
        exchange.apiResponse( persistence.executeQuery( Pet.class, Filters.eq( "userId", user.getId() ) ) );
    }
}

class AddPetToArea{
    private String petId;
    private String areaId;

    public String getPetId() { return petId; }

    public void setPetId(String petId) { this.petId = petId; }

    public String getAreaId() { return areaId; }

    public void setAreaId(String areaId) { this.areaId = areaId; }
}

class RegisterUser{
    private String email;
    private String name;
    private String password;

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }
}

class CreatePet{
    private String name;
    private String areaId;
    private String sensorId;

    public String getSensorId() { return sensorId; }

    public void setSensorId(String sensorId) { this.sensorId = sensorId; }

    public String getAreaId() { return areaId; }

    public void setAreaId(String areaId) { this.areaId = areaId; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }
}

class CreateArea{
    private float lat;
    private float lng;
    private String name;
    private float midLevelMeters;
    private float maxLevelMeters;

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public float getLat() { return lat; }

    public void setLat(float lat) { this.lat = lat; }

    public float getLng() { return lng; }

    public void setLng(float lng) { this.lng = lng; }

    public float getMidLevelMeters() { return midLevelMeters; }

    public void setMidLevelMeters(float midLevelMeters) { this.midLevelMeters = midLevelMeters; }

    public float getMaxLevelMeters() { return maxLevelMeters; }

    public void setMaxLevelMeters(float maxLevelMeters) { this.maxLevelMeters = maxLevelMeters; }
}