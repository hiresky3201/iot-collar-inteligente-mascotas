package me.iot.handlers;

import dev.morphia.query.experimental.filters.Filters;
import me.iot.entities.Metric;
import me.iot.entities.Pet;
import me.iot.entities.User;
import me.iot.persistence.Persistence;
import me.iot.udis.annotations.FromQuery;
import me.iot.udis.annotations.Route;
import me.iot.udis.enums.HttpMethod;
import me.iot.udis.wrappers.ServerExchange;
import org.apache.commons.math3.util.Pair;

import java.time.LocalDateTime;
import java.util.*;

public class MetricsHandlers {
    @Route( routes = "/metrics/pet", methods = HttpMethod.GET )
    public static void fetchPetMetrics(ServerExchange exchange, Persistence persistence, User user, @FromQuery( "id" ) String petId){
        Pet pet = persistence.getEntityById( Pet.class, petId );
        LocalDateTime min = LocalDateTime.now().toLocalDate().atTime( 0, 0 );

        List<Metric> metrics = persistence.executeQuery( Metric.class, Filters.eq( "relevantId", pet.getId() ), Filters.gte( "timestamp", min ) );
        exchange.apiResponse( metrics );
    }

    /*@Route( routes = "/metrics", methods = HttpMethod.POST )
    public static void fetchPetMetrics(ServerExchange exchange, Persistence persistence, User user, @FromJsonBody FetchPetMetrics fetchPetMetrics) {
        Pet pet = persistence.getEntityById( Pet.class, fetchPetMetrics.getPetId() );

        LocalDateTime min = LocalDateTime.ofInstant( Instant.parse( fetchPetMetrics.getDateTime() ) , ZoneId.systemDefault() );
        LocalDateTime max = min.plusHours( 5 );
        List<Metric> metrics = persistence.executeQuery( Metric.class, Filters.eq( "relevantId", pet.getId() ), Filters.gte( "timestamp", min ) );
        metrics = metrics.stream().filter( m -> m.getTimestamp().isBefore( max ) ).collect(Collectors.toList());

        // -- Getting linear model for the GPS positions during daytime
        List<Metric> batteryMetrics = metrics.stream().filter( x -> Metric.Type.BATTERY_LEVEL.equals( x.getType() ) ).collect( Collectors.toList() );
        List<Metric> areaMetrics = metrics.stream().filter( x -> Metric.Type.DISTANCE_TO_AREA_BORDER.equals( x.getType() ) ).collect( Collectors.toList() );

        Map<String, Object> model = new HashMap<>();
        model.put("batteryLevelModel", generateModelForBatteryLevels( fetchPetMetrics.getPetId(), batteryMetrics ) );
        model.put("distanceToAreaBorderModel", generateModelForAreaMetrics( fetchPetMetrics.getPetId(), areaMetrics ) );

        exchange.apiResponse(model);
    }

    private static LinearRegressionResult generateModelForBatteryLevels(String petId, List<Metric> batteryMetrics){
        List<Pair<Double, Double>> data = new ArrayList<>();

        batteryMetrics.forEach( m -> {
            double hourOfDay = m.getTimestamp().getHour();
            double batteryLevel = m.getDoubleValue();

            data.add( Pair.create( hourOfDay, batteryLevel ) );
        } );

        return getLinearRegressionModel( petId, "battery", data );
    }

    private static LinearRegressionResult generateModelForAreaMetrics(String petId, List<Metric> areaMetrics){
        List<Pair<Double, Double>> data = new ArrayList<>();

        areaMetrics.forEach( m -> {
            double hourOfDay = m.getTimestamp().getHour();
            double distanceToAreaBorder = m.getDoubleValue();

            data.add( Pair.create( hourOfDay, distanceToAreaBorder ) );
        } );

        return getLinearRegressionModel( petId, "distance", data );
    }

    private static LinearRegressionResult getLinearRegressionModel( String petId, String type, List<Pair<Double, Double>> data ) {
        if( data.isEmpty() ) return null;

        SimpleRegression sr = new SimpleRegression();

        for(Pair<Double, Double> d : data) {
            double x = d.getKey();
            double y = d.getValue();

            sr.addData(x,y);
        }

        double[] xs = data.stream().mapToDouble( Pair::getFirst ).toArray();
        double[] ys = data.stream().mapToDouble( Pair::getSecond ).toArray();

        // -- Building results
        LinearRegressionResult result = new LinearRegressionResult();
        result.setPetId( petId );

        result.setType( type );
        result.setR2( sr.getRSquare() );
        result.setStandardError( sr.getInterceptStdErr() );
        result.setCovariance( new Covariance().covariance( xs, ys ) );
        result.setPearsonCorrelation( new PearsonsCorrelation().correlation( xs, ys ) );

        // -- Making predictions based on X variable
        List<ModelPrediction> predictions = new ArrayList<>();

        Arrays.stream( xs ).forEach( x -> {
            ModelPrediction prediction = new ModelPrediction();
            prediction.setInput( x );
            prediction.setPrediction( sr.predict( x ) );
            predictions.add( prediction );
        } );

        result.setPredictions( predictions );

        return result;
    }*/
}

class LinearRegressionResult{
    private String petId;
    private String type;
    private double r2;
    private double covariance;
    private double standardError;
    private double pearsonCorrelation;
    private List<ModelPrediction> predictions;
    private List<Pair<Double, Double>> data;

    public String getPetId() { return petId; }

    public void setPetId(String petId) { this.petId = petId; }

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    public double getCovariance() { return covariance; }

    public void setCovariance(double covariance) { this.covariance = covariance; }

    public double getPearsonCorrelation() { return pearsonCorrelation; }

    public void setPearsonCorrelation(double pearsonCorrelation) { this.pearsonCorrelation = pearsonCorrelation; }

    public double getR2() { return r2; }

    public void setR2(double r2) { this.r2 = r2; }

    public double getStandardError() { return standardError; }

    public void setStandardError(double standardError) { this.standardError = standardError; }

    public List<ModelPrediction> getPredictions() { return predictions; }

    public void setPredictions(List<ModelPrediction> predictions) { this.predictions = predictions; }

    public List<Pair<Double, Double>> getData() { return data; }

    public void setData(List<Pair<Double, Double>> data) { this.data = data; }
}

class ModelPrediction{
    private double input;
    private double prediction;

    public double getInput() { return input; }

    public void setInput(double input) { this.input = input; }

    public double getPrediction() { return prediction; }

    public void setPrediction(double prediction) { this.prediction = prediction; }
}