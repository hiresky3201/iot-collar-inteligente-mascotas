package me.iot.entry;

import com.auth0.jwt.interfaces.DecodedJWT;
import me.iot.communication.monitor.MqttManager;
import me.iot.communication.monitor.MqttMessageManager;
import me.iot.communication.websocket.WebsocketManager;
import me.iot.entities.Metric;
import me.iot.entities.User;
import me.iot.exceptions.UnauthorizedException;
import me.iot.handlers.AlertHandlers;
import me.iot.handlers.AppHandlers;
import me.iot.handlers.AuthorizationHandlers;
import me.iot.handlers.MetricsHandlers;
import me.iot.persistence.Persistence;
import me.iot.udis.core.UndertowDependencyInjectionServer;
import me.iot.udis.middleware.CorsMiddleware;
import me.iot.utils.GeoUtils;

import java.time.LocalDateTime;
import java.util.Random;

public class Entry {
    public static void main(String[] args){
        try{
            // -- Run MQTT manager
            MqttManager.init( "tcp://0.0.0.0:1883", "AMBROSIA" );
            MqttManager.subscribe( MqttMessageManager.class, "GPS", "BATTERY" );

            // -- Run the websocket server
            WebsocketManager.init( "0.0.0.0", 4041 );

            // -- Dependency injection builder
            UndertowDependencyInjectionServer.builder()

            // -- Object Suppliers
            .addObjectSupplier( Persistence.class, ( e, p ) -> new Persistence() )
            .addObjectSupplier( DecodedJWT.class , AuthorizationHandlers::decodeToken )
            .addObjectSupplier( User.class , AuthorizationHandlers::verifyUser )

            // -- Exception profile messages
            .addExceptionStatusProfile( UnauthorizedException.class , 403, "No estas autorizado para acceder a esta ruta")

            // -- Handler classes
            .addHandlerClass( AppHandlers.class )
            .addHandlerClass( AlertHandlers.class )
            .addHandlerClass( AuthorizationHandlers.class )
            .addHandlerClass( MetricsHandlers.class )

            // -- Middleware go Here
            .addMiddleware( new CorsMiddleware() )
            .run( 4040 , "0.0.0.0" );

            //generateSimulatedMetricsForDistance();
            //generateSimulatedMetricsForBattery();
        }catch ( Exception e ){
            e.printStackTrace();
            System.exit( 1 );
        }
    }

    public static void generateSimulatedMetricsForBattery(){
        LocalDateTime accumulator = LocalDateTime.now();

        float initialBattery = 100.0f;
        float batteryLevel = initialBattery;

        Persistence persistence = new Persistence();
        Random random = new Random();

        for( int i = 0; i < (23 * 1); i++ ){
            // -- Submit metrics
            Metric metric = new Metric();
            metric.setRelevantId( "549a066f-3ee4-4521-b311-51450c32bc9d" );
            metric.setDoubleValue( batteryLevel );
            metric.setTimestamp( accumulator );
            metric.setType( Metric.Type.BATTERY_LEVEL );

            persistence.createEntity( metric );
            accumulator = accumulator.plusHours( 1 );
            batteryLevel -= random.nextFloat() * 5;

            if( batteryLevel <= 5 ) batteryLevel = 100;
        }
    }

    public static void generateSimulatedMetricsForDistance(){
        LocalDateTime accumulator = LocalDateTime.parse("2020-12-03T00:00:00");

        float initialLat = 29.025272369384766f;
        float initialLng = -110.94792938232422f;
        float lat = initialLat;
        float lng = initialLng;

        Persistence persistence = new Persistence();
        Random random = new Random();

        for( int i = 0; i < 23; i++ ){
            // -- Verifies if the pet is in a good area
            float distanceInMts = GeoUtils.haversine( initialLat, initialLng, lat, lng );

            // -- Submit metrics
            Metric metric = new Metric();
            metric.setRelevantId( "549a066f-3ee4-4521-b311-51450c32bc9d" );
            metric.setDoubleValue( distanceInMts );
            metric.setTimestamp( accumulator );
            metric.setType( Metric.Type.DISTANCE_TO_AREA_BORDER );

            persistence.createEntity( metric );
            accumulator = accumulator.plusHours( 1 );

            lat += ( random.nextInt(3) - 1 ) / 20000f;
            lng += ( random.nextInt(3) - 1 ) / 20000f;
        }
    }
}
