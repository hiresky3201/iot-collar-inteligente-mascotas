package me.iot.persistence;

import dev.morphia.query.experimental.filters.Filter;
import dev.morphia.query.experimental.filters.Filters;
import me.iot.entities.GenericEntity;
import me.iot.exceptions.InvalidEntityException;
import me.iot.persistence.morphia.MorphiaUtil;

import java.util.List;
import java.util.UUID;

public class Persistence {
    /**
     *
     */
    private static MorphiaUtil morphia;

    /**
     * Inicializa en la primera referencia
     */
    static {
        morphia = MorphiaUtil.getInstance();
        morphia.init( "mascotas-iot" );
/*        morphia.getDataStore().getDatabase().getCollection("mascotas").drop();
        morphia.getDataStore().getDatabase().getCollection("usuarios").drop();
        morphia.getDataStore().getDatabase().getCollection("areas").drop();*/
    }

    public <T extends GenericEntity> boolean createEntity(T entity){
        if( entity.getId() == null )
            entity.setId( UUID.randomUUID().toString() );

        return morphia.persist( entity );
    }

    public <T extends GenericEntity> boolean merge(T entity){
        return morphia.merge( entity );
    }

    public <T extends GenericEntity> boolean delete(T entity){
        return morphia.delete( entity );
    }

    /**
     * Regresa una entidad de la base de datos
     *
     * @param id La entidad con el ID a buscar.
     * @param tClass El tipo de clase a regresar
     * @return La mascota encontrado, si no se encuentra retornara nulo.
     */
    public <T extends GenericEntity> T getEntityById(Class<T> tClass, String id){
        return executeQuery( tClass, Filters.eq( "id", id ) ).stream().findFirst().orElseThrow( InvalidEntityException::new );
    }

    public <T extends GenericEntity> List<T> executeQuery( Class<T> tClass, Filter...filters ){
        return morphia.getDataStore().find( tClass ).filter( filters ).iterator().toList();
    }
}
