package me.iot.udis.exceptions;

public class InvalidMethodException extends RuntimeException {
    public InvalidMethodException(){
        super( "HttpMethod is not suited for this handler" );
    }
}
