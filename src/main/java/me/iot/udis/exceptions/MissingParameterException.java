package me.iot.udis.exceptions;

public class MissingParameterException extends RuntimeException {
    public MissingParameterException() {
    }

    public MissingParameterException(Exception e ){
        super( "One of the parameters needed for the handler is missing" , e );
    }
}
