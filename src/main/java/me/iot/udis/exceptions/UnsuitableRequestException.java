package me.iot.udis.exceptions;

public class UnsuitableRequestException extends RuntimeException {
    public UnsuitableRequestException() {
    }

    public UnsuitableRequestException( NumberFormatException e ){
        super( "The input of the request is not suitable for the handler" , e );
    }
}
