package me.iot.udis.middleware;

import io.undertow.server.HttpServerExchange;

public interface Middleware {
    boolean process(HttpServerExchange exchange);
}
