package me.iot.udis.enums;

public enum HttpMethod {
    GET , POST , PUT , HEAD , DELETE
}
