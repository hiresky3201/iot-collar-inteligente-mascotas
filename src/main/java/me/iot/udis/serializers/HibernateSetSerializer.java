package me.iot.udis.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.hibernate.Hibernate;
import org.hibernate.collection.internal.PersistentSet;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class HibernateSetSerializer extends JsonSerializer<Set> {
    @Override
    @SuppressWarnings( "unchecked" )
    public void serialize(Set set, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        // --- Checking to see if the set is tied to a proxy
        // If so, we must break ties with hibernate to prevent persistence errors
        if(set instanceof PersistentSet){
            // --- Set is a proxy. We must check if it is initialized.
            // If so, we must unproxy each of its children, and the set itself
            if( Hibernate.isInitialized( set ) ){
                set = new HashSet( set );
                set.forEach( Hibernate::unproxy );
            }
            // --- Else, just set as empty HashSet
            else{
                set = new HashSet();
            }
        }

        // --- Write json
        jsonGenerator.writeStartArray();

        for (Object o : set) {
            jsonGenerator.writeObject( o );
        }

        jsonGenerator.writeEndArray();
    }

    @Override
    public Class<Set> handledType() {
        return Set.class;
    }
}
