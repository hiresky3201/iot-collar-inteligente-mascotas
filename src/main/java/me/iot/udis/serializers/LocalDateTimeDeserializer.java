package me.iot.udis.serializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.*;

public class LocalDateTimeDeserializer extends StdDeserializer<LocalDateTime> {
    private static final ZoneOffset ZONE_OFFSET = ZonedDateTime.now().getOffset();
    public LocalDateTimeDeserializer(){
        this( null );
    }
    public LocalDateTimeDeserializer(Class<LocalDateTime> t) {
        super(t);
    }

    @Override
    public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        String v = jsonParser.getValueAsString();
        Instant i = Instant.parse( v );
        return LocalDateTime.ofInstant( i , ZoneId.systemDefault() );
    }
}
