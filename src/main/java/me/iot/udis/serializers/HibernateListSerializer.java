package me.iot.udis.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.hibernate.Hibernate;
import org.hibernate.collection.internal.PersistentBag;
import org.hibernate.collection.internal.PersistentList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HibernateListSerializer extends JsonSerializer<List> {
    @Override
    @SuppressWarnings( "unchecked" )
    public void serialize(List list, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        // --- Checking to see if the list is tied to a proxy
        // If so, we must break ties with hibernate to prevent persistence errors
        if(list instanceof PersistentList || list instanceof PersistentBag){
            // --- Set is a proxy. We must check if it is initialized.
            // If so, we must unproxy each of its children, and the list itself
            if( Hibernate.isInitialized( list ) ){
                list = new ArrayList( list );
                list.forEach( Hibernate::unproxy );
            }
            // --- Else, just set as empty HashSet
            else return;
        }

        // --- Write json
        jsonGenerator.writeStartArray();

        for (Object o : list) {
            jsonGenerator.writeObject( o );
        }

        jsonGenerator.writeEndArray();
    }

    @Override
    public Class<List> handledType() {
        return List.class;
    }
}
