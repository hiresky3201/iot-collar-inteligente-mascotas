package me.iot.udis.annotations;

import me.iot.udis.enums.HttpMethod;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target( ElementType.METHOD )
@Retention( RetentionPolicy.RUNTIME )
public @interface Route {
    String[] routes() default {};
    String regex() default "";
    HttpMethod[] methods() default {};
}
