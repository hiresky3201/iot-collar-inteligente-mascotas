package me.iot.udis.wrappers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.io.JsonEOFException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import com.fasterxml.jackson.databind.module.SimpleModule;
import io.undertow.io.IoCallback;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.Cookie;
import io.undertow.server.handlers.CookieImpl;
import io.undertow.util.HttpString;
import io.undertow.util.StatusCodes;
import me.iot.exceptions.MalformedJsonException;
import me.iot.exceptions.MissingRequestBodyException;
import me.iot.udis.serializers.*;
import org.hibernate.proxy.pojo.bytebuddy.ByteBuddyInterceptor;

import io.undertow.util.Headers;
import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

public class ServerExchange {
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final Date NEXT_YEAR = Date.from( Instant.now().plus( 365 , ChronoUnit.DAYS ) );
    private static final Date LAST_YEAR = Date.from( Instant.now().plus( -365 , ChronoUnit.DAYS ) );
    private static final Object INSTANCE = new Object();
    private HttpServerExchange exchange;
    private HashMap<String,Object> session;

    static {
        SimpleModule module = new SimpleModule();
        module.addSerializer( LocalDate.class , new LocalDateSerializer() );
        module.addSerializer( LocalDateTime.class , new LocalDateTimeSerializer() );
        module.addSerializer( ByteBuddyInterceptor.class , new ByteBuddyInterceptorSerializer());
        module.addDeserializer( LocalDate.class , new LocalDateDeserializer() );
        module.addDeserializer( LocalDateTime.class , new LocalDateTimeDeserializer() );

        OBJECT_MAPPER.registerModule( module );
    }

    public ServerExchange( HttpServerExchange exchange ) {
        this.exchange = exchange;
    }

    public void setStatusCode( int statusCode ){
        exchange.setStatusCode( statusCode );
    }

    public HashMap<String, Object> getSession() {
        if( session == null ) session = new HashMap<>();
        return session;
    }

    public HttpString getRequestMethod(){
        return exchange.getRequestMethod();
    }

    public String getRequestPath(){
        return exchange.getRequestPath();
    }

    public int getResponseStatus(){
        return exchange.getStatusCode();
    }

    public void setCookie(String cookieName , String cookieValue , String domain ){
        Cookie cookie = new CookieImpl( cookieName , cookieValue );
        cookie.setPath("/");
        cookie.setDomain( domain );
        cookie.setExpires( NEXT_YEAR );
        cookie.setSameSite( true );
        exchange.setResponseCookie( cookie );
    }

    public void setCookie( String cookieName , String cookieValue , String domain , String sameSite , boolean httpOnly ){
        Cookie cookie = new CookieImpl( cookieName , cookieValue );
        cookie.setPath("/");
        cookie.setDomain( domain );
        cookie.setExpires( NEXT_YEAR );
        cookie.setSameSiteMode( sameSite );
        cookie.setHttpOnly( httpOnly );
        exchange.setResponseCookie( cookie );
    }

    public void setCookie( String cookieName , String cookieValue ){
        setCookie( cookieName , cookieValue , String.format( "%s" , this.exchange.getHostName() ) );
    }

    public void expireCookie( String cookieName ){
        Cookie cookie = new CookieImpl( cookieName , null );
        cookie.setPath("/");
        cookie.setDomain( String.format( "%s" , this.exchange.getHostName() ) );
        cookie.setExpires( LAST_YEAR );
        exchange.setResponseCookie( cookie );
    }

    private void endJsonExchange( Object object ){
        byte[] body = null;
        try {
            body = OBJECT_MAPPER.writeValueAsBytes(  object == null ? INSTANCE : object  );
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while serializing object -> json" , e );
        }
        exchange.getResponseHeaders().put( Headers.CONTENT_TYPE, "application/json");
        exchange.getResponseSender().send( ByteBuffer.wrap( body ) , IoCallback.END_EXCHANGE );
    }

    public void json( Map<String,Object> object ){ endJsonExchange( object );}

    public void json( ApiResponse object ){
        endJsonExchange( object );
    }

    public <T> void apiResponse( T object ){
        json( new ApiResponse<T>( object ) );
    }

    public void text( String object ){
        exchange.getResponseHeaders().put( Headers.CONTENT_TYPE, "text/plain");
        exchange.getResponseSender().send( object );
        exchange.endExchange();
    }

    public void html( String object ){
        exchange.getResponseHeaders().put( Headers.CONTENT_TYPE, "text/html");
        exchange.getResponseSender().send( object );
        exchange.endExchange();
    }

    public void header( HttpString headerName , String headerValue ){
        exchange.getResponseHeaders().put( headerName , headerValue );
    }

    public void header( HttpString headerName , Long headerValue ){
        exchange.getResponseHeaders().put( headerName , headerValue );
    }

    public void raw( String contentType , byte[] bytes ){
        exchange.getResponseHeaders().put( Headers.CONTENT_TYPE, contentType );
        exchange.getResponseSender().send( ByteBuffer.wrap( bytes ) );
        exchange.endExchange();
    }

    public void redirect( URL url , int status ){
        exchange.setStatusCode( status );
        exchange.getResponseHeaders().put( Headers.LOCATION, url.toString() );
        exchange.endExchange();
    }

    public void redirect( URL url ){
        redirect( url , StatusCodes.FOUND );
    }

    public void raw( String contentType , Path path ) throws IOException {
        raw( contentType ,  Files.readAllBytes( path ) );
    }

    public Set<String> getQueryParameterNames(){
        return exchange.getQueryParameters().keySet();
    }

    public String getQueryParameterValue( String queryParameterName ){
        Deque<String> queue = exchange.getQueryParameters().get( queryParameterName );
        if( queue.isEmpty() ) return null;
        return queue.getFirst();
    }

    public Set<String> getHeadersNames(){
        return exchange.getRequestHeaders().getHeaderNames().stream().map( HttpString::toString ).collect( Collectors.toSet() );
    }

    public String getHeaderValue( String headerName ){
        return exchange.getRequestHeaders().getFirst( headerName );
    }

    public static <T> T readValue( byte[] input , Class<T> tClass ){
        try {
            if( input.length == 0 ) throw new MissingRequestBodyException();
            return OBJECT_MAPPER.readValue( input , tClass );
        } catch (IOException e) {
            if( e instanceof JsonEOFException ) throw new MalformedJsonException();
            if( e instanceof UnrecognizedPropertyException) throw new MalformedJsonException( String.format( "%s is not a recognized field", ((UnrecognizedPropertyException) e).getPropertyName() ) );
            throw new RuntimeException("Unable to parse input -> tClass", e);
        }
    }

    public static String writeValueAsString( Object object ){
        try {
            return OBJECT_MAPPER.writeValueAsString( object );
        } catch (IOException e) {
            throw new RuntimeException("Unable to serialize object -> string",e);
        }
    }


}
