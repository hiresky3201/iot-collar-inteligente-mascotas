package me.iot.udis.wrappers;

import java.util.Arrays;
import java.util.List;

public class ApiResponse<T> {
    private T data;
    private String exceptionType;
    private List<String> errors;
    private List<String> warnings;
    private boolean isError;

    public ApiResponse() {
        this.isError = false;
    }

    public ApiResponse( T data ) {
        this.data = data;
        this.isError = false;
    }

    public ApiResponse( Throwable exception ) {
        this( exception , exception.getMessage() );
    }

    public ApiResponse( Throwable exception , String message ) {
        this.errors = Arrays.asList( message );
        this.exceptionType = exception.getClass().getCanonicalName();
        this.isError = true;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getExceptionType() {
        return exceptionType;
    }

    public void setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public List<String> getWarnings() {
        return warnings;
    }

    public void setWarnings(List<String> warnings) {
        this.warnings = warnings;
    }

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }
}
