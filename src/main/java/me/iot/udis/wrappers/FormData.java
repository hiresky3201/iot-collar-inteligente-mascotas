package me.iot.udis.wrappers;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Optional;
import java.util.function.Function;

public class FormData {
    private HashMap<String,String> values = new HashMap<>();

    public FormData() {
    }

    public FormData( String body ){
        if( body == null )return;
        String[] tokens = body.split("&|=");
        try {
            for (int i = 0; i < tokens.length - 1 ; i-=-2 ) {
                values.put( URLDecoder.decode( tokens[i] , "UTF-8" ) , URLDecoder.decode( tokens[i+1] , "UTF-8" ) );
            }
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException("Invalid form format",e);
        }
    }

    public Optional<String> get( String key ){
        if( !values.containsKey( key ) ) return Optional.empty();
        return Optional.ofNullable( values.get( key ) );
    }

    public <T> Optional<T> get( String key , Function<String,T> parser ){
        if( !values.containsKey( key ) ) return Optional.empty();
        return Optional.ofNullable( parser.apply( values.get( key ) ) );
    }

}
