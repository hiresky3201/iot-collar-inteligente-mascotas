package me.iot.udis.wrappers;

import io.undertow.server.HttpServerExchange;

import java.util.Deque;
import java.util.Map;
import java.util.Set;

public class QueryString{
    private Map<String, Deque<String>> queryString;

    public QueryString( HttpServerExchange exchange ){
        this.queryString = exchange.getQueryParameters();
    }

    public Map<String, Deque<String>> getQueryString() {
        return queryString;
    }

    public void setQueryString(Map<String, Deque<String>> queryString) {
        this.queryString = queryString;
    }

    public int size() {
        return this.queryString.size();
    }

    public boolean isEmpty() {
        return this.queryString.isEmpty();
    }

    public boolean containsKey(Object key) {
        return this.queryString.containsKey( key );
    }

    public String get(Object key) {
        return this.queryString.get( key ).getFirst();
    }

    public Set<String> keySet() {
        return this.queryString.keySet();
    }

    public String getOrDefault(Object key, String defaultValue) {
        if( !this.queryString.containsKey( key ) ) return defaultValue;
        return this.get( key );
    }
}
