package me.iot.udis.core;

import java.lang.reflect.Parameter;

public class HandlersParameterArray implements AutoCloseable {
    private Object[] parameters;
    private Parameter[] rawParameters;

    public HandlersParameterArray(int size) {
        this.parameters = new Object[ size ];
        this.rawParameters = new Parameter[ size ];
    }

    public <T> T get( Class<T> tClass ){
        if( tClass == null ) return null;
        for (Object o : parameters ) {
            if( o == null ) continue;
            if( tClass.isAssignableFrom( o.getClass() ) ) return (T) o;
        }
        return null;
    }

    public Parameter getRaw( Class tClass ){
        if( tClass == null ) return null;
        for (Parameter o : rawParameters ) {
            if( o == null ) continue;
            if( tClass.equals( o.getParameterizedType() ) ) return o;
        }
        return null;
    }

    public <T> T get(int index){
        return (T) parameters[index];
    }

    public void put(int index , Object o){ parameters[index] = o; }

    public void setRawParameters(Parameter[] rawParameters){ this.rawParameters = rawParameters; }

    public Object[] getParameters() {
        return parameters;
    }

    @Override
    public void close() throws Exception {
        for ( Object arg : this.parameters ) {
            if( !(arg instanceof AutoCloseable) ) continue;
            ((AutoCloseable) arg).close();
        }
    }
}
