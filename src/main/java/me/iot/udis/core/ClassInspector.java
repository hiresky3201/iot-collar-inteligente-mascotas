package me.iot.udis.core;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ClassInspector {

    public static List<Field> getAllFieldsOf( Class tClass , Predicate<Field> predicate ){
        if( tClass == null ) return Collections.emptyList();
        List<Field> r = new ArrayList<>();

        while( !tClass.equals( Object.class ) ){
            List<Field> fields = Arrays.asList( tClass.getDeclaredFields() );
            fields.forEach( x -> x.setAccessible( true ) );
            r.addAll( fields );
            tClass = tClass.getSuperclass();
        }

        return r.stream().filter( x -> !Modifier.isTransient( x.getModifiers() ) && !Modifier.isStatic( x.getModifiers() ) ).filter( predicate ).collect( Collectors.toList() );
    }

    public static List<Method> getAllMethodsOf( Class tClass , Predicate<Method> predicate ){
        if( tClass == null ) return Collections.emptyList();

        List<Method> r = new ArrayList<>();

        while( !tClass.equals( Object.class ) ){
            List<Method> fields = Arrays.asList( tClass.getDeclaredMethods() );
            r.forEach( x -> x.setAccessible( true ) );
            r.addAll( fields );
            tClass = tClass.getSuperclass();
        }

        return r.stream().filter( x -> !Modifier.isStatic( x.getModifiers() ) ).filter( predicate ).collect( Collectors.toList() );
    }
}
