package me.iot.udis.core;

import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import me.iot.udis.annotations.Route;
import me.iot.udis.middleware.Middleware;
import me.iot.udis.wrappers.ApiResponse;
import me.iot.udis.wrappers.Headers;
import me.iot.udis.wrappers.QueryString;
import me.iot.udis.wrappers.ServerExchange;
import org.apache.commons.lang3.tuple.Pair;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UndertowDependencyInjectionServer implements HttpHandler {
    private final List<Middleware> MIDDLEWARE = new ArrayList<>();
    private final ConcurrentHashMap<String,Handler> stringRoutes = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Pattern,Handler> regexRoutes = new ConcurrentHashMap<>();

    private final ConcurrentHashMap<Class, BiFunction> objectSuppliers = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Class<? extends Exception>, Pair<Integer,String>> exceptionToStatusCodeMapper = new ConcurrentHashMap<>();
    private Consumer<Throwable> exceptionListener;
    private Consumer<ServerExchange> beforeHandler;
    private BiConsumer<ServerExchange,Throwable> afterHandler;

    private UndertowDependencyInjectionServer() {
        super();
        this.addObjectSupplier( ServerExchange.class , UndertowDependencyInjectionServer::toServerExchange );
        this.addObjectSupplier( QueryString.class , UndertowDependencyInjectionServer::toQueryString );
        this.addObjectSupplier( Headers.class , UndertowDependencyInjectionServer::toHeaders );
        this.addExceptionStatusProfile( Exception.class , 500 , "An undetermined error has happened on the server" );
        this.addExceptionStatusProfile( NumberFormatException.class , 422 , "One of the specified parameters has the wrong format" );
    }

    public static UndertowDependencyInjectionServer builder(){
        return new UndertowDependencyInjectionServer();
    }

    public UndertowDependencyInjectionServer addMiddleware( Middleware middleware ){
        this.MIDDLEWARE.add( middleware );
        return this;
    }

    public <T> UndertowDependencyInjectionServer addHandlerClass(Class<T> tClass ){
        if( tClass == null ) return this;
        Method[] methods = Arrays.stream( tClass.getDeclaredMethods() )
                               .filter( method -> void.class.equals( method.getReturnType() )
                                                  && Modifier.isStatic( method.getModifiers() )
                                                  && method.isAnnotationPresent( Route.class )
                               ).filter( method -> Arrays.asList( method.getParameterTypes() ).contains( ServerExchange.class ) )
                               .toArray( Method[]::new );
        for ( Method method : methods ) {
            try {
                Route routeAnnotation = method.getAnnotation( Route.class );
                if( routeAnnotation.routes().length > 0 ){
                    Handler handler = new Handler( this, method , routeAnnotation );
                    for ( String route : routeAnnotation.routes() ) {
                        stringRoutes.put( route.toLowerCase() , handler );
                    }
                }else if( !routeAnnotation.regex().isEmpty() ){
                    regexRoutes.put( Pattern.compile( routeAnnotation.regex() ) , new Handler( this, method , routeAnnotation ) );
                }
            } catch (Exception ignored) {
            }
        }
        return this;
    }

    public <T> UndertowDependencyInjectionServer addObjectSupplier(Class<T> tClass , BiFunction<HttpServerExchange,HandlersParameterArray,T> function ){
        this.objectSuppliers.put( tClass , function );
        return this;
    }

    public <T extends Exception> UndertowDependencyInjectionServer addExceptionStatusProfile(Class<T> tClass , int statusCode , String message ){
        this.exceptionToStatusCodeMapper.put( tClass , Pair.of( statusCode , message ) );
        return this;
    }

    public <T extends Exception> UndertowDependencyInjectionServer addExceptionStatusProfile(Class<T> tClass , int statusCode ){
        this.exceptionToStatusCodeMapper.put( tClass , Pair.of( statusCode , null ) );
        return this;
    }

    public UndertowDependencyInjectionServer addExceptionListener(Consumer<Throwable> exceptionListener) {
        this.exceptionListener = exceptionListener;
        return this;
    }

    public Consumer<Throwable> getExceptionListener() {
        return exceptionListener;
    }

    public ConcurrentHashMap<Class, BiFunction> getObjectSuppliers() {
        return objectSuppliers;
    }

    public ConcurrentHashMap<Class<? extends Exception>, Pair<Integer,String>> getExceptionToStatusCodeMapper() {
        return exceptionToStatusCodeMapper;
    }

    public Consumer<ServerExchange> getBeforeHandler() {
        return beforeHandler;
    }

    public UndertowDependencyInjectionServer addBeforeHandler(Consumer<ServerExchange> beforeHandler) {
        this.beforeHandler = beforeHandler;
        return this;
    }

    public boolean hasBeforeHandler(){
        return this.beforeHandler != null;
    }

    public BiConsumer<ServerExchange, Throwable> getAfterHandler() {
        return afterHandler;
    }

    public UndertowDependencyInjectionServer addAfterHandler(BiConsumer<ServerExchange, Throwable> afterHandler) {
        this.afterHandler = afterHandler;
        return this;
    }

    public boolean hasAfterHandler(){
        return this.afterHandler != null;
    }

    @Override
    public void handleRequest( HttpServerExchange exchange ) throws Exception {
        Handler handler = null ;
        if( stringRoutes.containsKey( exchange.getRequestPath() ) ){
            handler = stringRoutes.get( exchange.getRequestPath() );
        }else{
            for ( Map.Entry<Pattern, Handler> entry : regexRoutes.entrySet() ) {
                Matcher matcher = entry.getKey().matcher( exchange.getRequestPath() );
                if( matcher.matches() ){
                    handler = entry.getValue();
                    break;
                }
            }
        }
        if( handler == null ){
            exchange.setStatusCode( 404 );
            exchange.getResponseHeaders().put( io.undertow.util.Headers.CONTENT_TYPE, "application/json");
            exchange.getResponseSender().send( ServerExchange.writeValueAsString( new ApiResponse( new RuntimeException( "Unable to find a suitable handler for the request" ) ) ) );
            exchange.endExchange();

            throw new RuntimeException( "Unable to find a suitable handler for the request" );
        }else{
            boolean terminatedByMiddleWare = false;
            for ( Middleware middleware : this.MIDDLEWARE ) {
                if( terminatedByMiddleWare ) break;
                terminatedByMiddleWare = middleware.process( exchange );
            }
            if( !terminatedByMiddleWare ) handler.handleRequest( exchange );
        }
    }

    public void run(){
        run( 9898 , "127.0.0.1" );
    }

    public void run( int port , String host ){
        Undertow
        .builder()
        .addHttpListener( port , host )
        .setHandler( this )
        .build()
        .start();
    }

    public static ServerExchange toServerExchange( HttpServerExchange httpServerExchange , HandlersParameterArray handlersParameterArray ){
        return new ServerExchange( httpServerExchange );
    }

    public static QueryString toQueryString( HttpServerExchange httpServerExchange, HandlersParameterArray handlersParameterArray ){
        return new QueryString( httpServerExchange );
    }

    public static Headers toHeaders( HttpServerExchange httpServerExchange, HandlersParameterArray handlersParameterArray ){
        return new Headers( httpServerExchange );
    }

}
