package me.iot.entities;

import dev.morphia.annotations.Entity;

import java.time.LocalDateTime;

@Entity( "alertas" )
public class Alert extends GenericEntity{
    public enum Method{ EMAIL, WEBSOCKET }
    public enum Type{
        GPS_MAX,
        GPS_MID,
        BATTERY
    }

    private String userId;
    private String sensorId;
    private Type type;
    private Method method;
    private double threshold;
    private boolean isActive;
    private int minutesBetweenAlertNotifications;
    private LocalDateTime lastAlertNotification;

    public double getThreshold() { return threshold; }

    public void setThreshold(double threshold) { this.threshold = threshold; }

    public String getSensorId() { return sensorId; }

    public void setSensorId(String sensorId) { this.sensorId = sensorId; }

    public String getUserId() { return userId; }

    public void setUserId(String userId) { this.userId = userId; }

    public Type getType() { return type; }

    public void setType(Type type) { this.type = type; }

    public Method getMethod() { return method; }

    public void setMethod(Method method) { this.method = method; }

    public boolean isActive() { return isActive; }

    public void setActive(boolean active) { isActive = active; }

    public LocalDateTime getLastAlertNotification() { return lastAlertNotification; }

    public void setLastAlertNotification(LocalDateTime lastAlertNotification) { this.lastAlertNotification = lastAlertNotification; }

    public int getMinutesBetweenAlertNotifications() { return minutesBetweenAlertNotifications; }

    public void setMinutesBetweenAlertNotifications(int minutesBetweenAlertNotifications) { this.minutesBetweenAlertNotifications = minutesBetweenAlertNotifications; }
}
