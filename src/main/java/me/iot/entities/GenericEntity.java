package me.iot.entities;

import dev.morphia.annotations.Id;

public class GenericEntity {
    @Id
    private String id;

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }
}
