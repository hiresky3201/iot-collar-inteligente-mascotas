package me.iot.entities;

import dev.morphia.annotations.Entity;

import java.time.LocalDateTime;

@Entity( "logs" )
public class Log extends GenericEntity {
    private String userId;
    private String description;
    private LocalDateTime timestamp;

    public String getUserId() { return userId; }

    public void setUserId(String userId) { this.userId = userId; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public LocalDateTime getTimestamp() { return timestamp; }

    public void setTimestamp(LocalDateTime timestamp) { this.timestamp = timestamp; }
}
