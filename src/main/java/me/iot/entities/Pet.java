package me.iot.entities;

import dev.morphia.annotations.Entity;

@Entity("mascotas")
public class Pet extends GenericEntity {
    private String name;
    private String userId;
    private String areaId;
    private String sensorId;
    private float lat;
    private float lng;

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getUserId() { return userId; }

    public void setUserId(String userId) { this.userId = userId; }

    public float getLat() { return lat; }

    public void setLat(float lat) { this.lat = lat; }

    public float getLng() { return lng; }

    public void setLng(float lng) { this.lng = lng; }

    public String getAreaId() { return areaId; }

    public void setAreaId(String areaId) { this.areaId = areaId; }

    public String getSensorId() { return sensorId; }

    public void setSensorId(String sensorId) { this.sensorId = sensorId; }
}
