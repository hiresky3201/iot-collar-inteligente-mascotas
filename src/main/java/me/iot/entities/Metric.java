package me.iot.entities;

import dev.morphia.annotations.Entity;

import java.time.LocalDateTime;

@Entity( "metrics" )
public class Metric extends GenericEntity{
    public enum Type {
        DISTANCE_TO_AREA_BORDER,
        BATTERY_LEVEL
    }

    private Type type;
    private String value;
    private String relevantId;
    private LocalDateTime timestamp;
    private int intValue;
    private double doubleValue;
    private boolean booleanValue;

    public Type getType() { return type; }

    public void setType(Type type) { this.type = type; }

    public String getValue() { return value; }

    public void setValue(String value) { this.value = value; }

    public String getRelevantId() { return relevantId; }

    public void setRelevantId(String relevantId) { this.relevantId = relevantId; }

    public LocalDateTime getTimestamp() { return timestamp; }

    public void setTimestamp(LocalDateTime timestamp) { this.timestamp = timestamp; }

    public int getIntValue() { return intValue; }

    public void setIntValue(int intValue) { this.intValue = intValue; }

    public double getDoubleValue() { return doubleValue; }

    public void setDoubleValue(double doubleValue) { this.doubleValue = doubleValue; }

    public boolean isBooleanValue() { return booleanValue; }

    public void setBooleanValue(boolean booleanValue) { this.booleanValue = booleanValue; }
}
