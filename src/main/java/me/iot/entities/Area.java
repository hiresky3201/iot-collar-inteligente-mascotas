package me.iot.entities;

import dev.morphia.annotations.Entity;

import java.time.LocalDateTime;

@Entity("areas")
public class Area extends GenericEntity {
    private String userId;
    private String name;
    private float lat;
    private float lng;
    private float midLevelMeters;
    private float maxLevelMeters;
    private LocalDateTime lastNotification;

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public float getLat() { return lat; }

    public void setLat(float lat) { this.lat = lat; }

    public float getLng() { return lng; }

    public void setLng(float lng) { this.lng = lng; }

    public String getUserId() { return userId; }

    public void setUserId(String userId) { this.userId = userId; }

    public float getMidLevelMeters() { return midLevelMeters; }

    public void setMidLevelMeters(float midLevelMeters) { this.midLevelMeters = midLevelMeters; }

    public float getMaxLevelMeters() { return maxLevelMeters; }

    public void setMaxLevelMeters(float maxLevelMeters) { this.maxLevelMeters = maxLevelMeters; }

    public LocalDateTime getLastNotification() { return lastNotification; }

    public void setLastNotification(LocalDateTime lastNotification) { this.lastNotification = lastNotification; }
}
