package me.iot.utils;

import me.iot.utils.enums.EmailContentType;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

public class EmailUtil {

    // This address must be verified.
    static final String FROMNAME = "IOT-MASCOTAS";
    public static final String FROM = "hiresky3201@gmail.com";

    // Replace smtp_username with your Amazon SES SMTP user name.
    static final String SMTP_USERNAME = "raul.sabag@potros.itson.edu.mx";

    // Replace smtp_password with your Amazon SES SMTP password.
    static final String SMTP_PASSWORD = "AlienshOOter1234";

    // The SMTP Host.
    static final String HOST = "smtp.gmail.com";

    // The port you will connect to on the Amazon SES SMTP endpoint.
    static final int PORT = 587;

    static final String TEXT_PLAIN_MIME = "text/plain; charset=UTF-8";

    static final String HTML_MIME = "text/html; charset=UTF-8";

    /**
     * Utility method to send an email with HTML type content.
     *
     * @param toEmail The target email to send to.
     * @param subject The subject of this email.
     * @param body    The body of this email address.
     * @param type Sends an email with a determined type.
     */
    public static void sendEmail(String toEmail, String subject, String body, EmailContentType type , String fileAttachment ) {
        Transport transport = null;

        try {
            // Create a Properties object to contain connection configuration information.
            Properties props = System.getProperties();
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.port", PORT);
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.auth", "true");

            // Create a Session object to represent a mail session with the specified properties.
            Session session = Session.getDefaultInstance( props );

            // To eliminate all special characters and white spaces causing question marks
            // to appear in the email client.
            //This should only apply for html type emails.


            // Create a message with the specified information.
            MimeMessage msg = new MimeMessage( session );

            msg.setFrom( new InternetAddress( FROM,  "IOT-Mascotas" ) );
            msg.setRecipient( Message.RecipientType.TO, new InternetAddress( toEmail ) );
            msg.setSubject(subject, "UTF-8");

            Multipart multipart = new MimeMultipart();

            MimeBodyPart b = new MimeBodyPart();
            b.setContent( body , EmailContentType.HTML_TEXT.equals( type ) ? HTML_MIME : TEXT_PLAIN_MIME );

            multipart.addBodyPart( b );

            if( fileAttachment != null ){
                MimeBodyPart attachedFilePart = new MimeBodyPart();
                DataSource source = new FileDataSource(fileAttachment);
                attachedFilePart.setDataHandler(new DataHandler(source));
                attachedFilePart.setFileName( fileAttachment.substring( fileAttachment.lastIndexOf( "/" ) + 1 ) );
                multipart.addBodyPart(attachedFilePart);
            }

            msg.setContent(multipart);

            // Create a transport.
            transport = session.getTransport();

            // Send the message.
            System.out.println("Sending...");

            // Connect to Amazon SES using the SMTP username and password.
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);

            // Send the email.
            transport.sendMessage(msg, msg.getAllRecipients());

            System.out.println("Email sent!");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("The email was not sent.");
            System.out.println("Error message: " + e.getMessage());
        } finally {
            // Close and terminate the connection.
            if( transport != null ) {
                try {
                    transport.close();
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
