package me.iot.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.util.Collection;

public class JacksonUtil {
//    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper().registerModule( new JavaTimeModule() );
    private static ObjectMapper OBJECT_MAPPER;

    public static ObjectMapper getObjectMapper(){
        if( OBJECT_MAPPER != null ) return OBJECT_MAPPER;
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        OBJECT_MAPPER = objectMapper;
        return OBJECT_MAPPER;
    }

    public static <T> T readValue(byte[] input , Class<T> tClass ){
        try {
            return getObjectMapper().readValue( input , tClass );
        } catch (IOException e) {
            throw new RuntimeException("Unable to parse input -> tClass",e);
        }
    }

    public static <T> T tryReadValue( String input , Class<T> tClass ){
        try {
            return getObjectMapper().readValue( input , tClass );
        } catch (IOException e) {
            return null;
        }
    }

    public static <T> T readValue( String input , Class<T> tClass ){
        try {
            return getObjectMapper().readValue( input , tClass );
        } catch (IOException e) {
            throw new RuntimeException("Unable to parse input -> tClass",e);
        }
    }

    public static <T> T readValue( String data, TypeReference<T> typeReference ) {
        try {
            return getObjectMapper().readValue( data , typeReference );
        } catch (IOException e) {
            throw new RuntimeException("Unable to parse input -> tClass",e);
        }
    }

    public static <T> T readValue(String data, Class<? extends Collection> tClass , Class genericClass ) {
        try {
            JavaType type = getObjectMapper().getTypeFactory().constructCollectionType( tClass, genericClass );
            return getObjectMapper().readValue( data , type );
        } catch (IOException e) {
            throw new RuntimeException("Unable to parse input -> tClass",e);
        }
    }

    public static String writeValueAsString( Object object ){
        try {
            return getObjectMapper().writeValueAsString( object );
        } catch (IOException e) {
            throw new RuntimeException("Unable to serialize object -> string",e);
        }
    }

}
