package me.iot.utils.enums;

public enum EmailContentType {
    PLAIN_TEXT("text/plain"),
    HTML_TEXT("text/html");

    String type;

    EmailContentType(String type){
        this.type = type;
    }

    public String getContentType(){ return type; }
}
