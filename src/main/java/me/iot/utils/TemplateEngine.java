package me.iot.utils;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Helper;
import spark.template.handlebars.HandlebarsTemplateEngine;

public class TemplateEngine extends HandlebarsTemplateEngine {
    public TemplateEngine() {
        super("/templates");

        this.handlebars.getCache().clear();
        this.handlebars.getCache().setReload(true);
    }

    public TemplateEngine(String resourceRoot) {
        super(resourceRoot);
    }

    public Handlebars getHandlebarsInstance(){
        return this.handlebars;
    }

    public <H> void registerHelper( String name , Helper<H> a ){
        this.handlebars.registerHelper( name , a );
    }
}
