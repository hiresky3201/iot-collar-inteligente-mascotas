package me.iot.utils;

import java.util.Arrays;
import java.util.Objects;
import java.util.regex.Pattern;

public abstract class StringUtils {
    public static final String EMPTY = "";
    public final static String EMPTY_GUID = "00000000-0000-0000-0000-000000000000";
    private static Pattern p = Pattern.compile("^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f‌​]{4}-[0-9a-f]{12}$");

    public static String ensureString( Object input, String...fallback){
        if( input != null && !StringUtils.isEmpty( input.toString() ) ) return input.toString();

        //Ensure this fails if no string given instead of returning an empty string.
        return Arrays.stream( fallback ).filter(Objects::nonNull).findFirst().orElse( null );
    }

    public static String ensureString( Object input, Class<? extends Exception> e) throws Exception {
        if( input == null || StringUtils.isEmpty( input.toString() ) ) throw e.newInstance();

        //Ensure this fails if no string given instead of returning an empty string.
        return input.toString();
    }

    public static boolean equalsIgnoreCaseToAny( Object input , String...inputs){
        if( input == null ) return false;
        return Arrays.stream( inputs ).filter(Objects::nonNull).anyMatch( x -> x.equalsIgnoreCase( input.toString() ) );
    }

    public static boolean areAnyEmpty( String...inputs ){
        return Arrays.stream( inputs ).anyMatch( StringUtils::isEmpty );
    }

    public static String between(String value, String a, String b) {
        // Return a substring between the two strings.
        int posA = value.indexOf(a);
        if (posA == -1) {
            return "";
        }
        int posB = value.lastIndexOf(b);
        if (posB == -1) {
            return "";
        }
        int adjustedPosA = posA + a.length();
        if (adjustedPosA >= posB) {
            return "";
        }
        return value.substring(adjustedPosA, posB);
    }

    public static String before(String value, String a) {
        // Return substring containing all characters before a string.
        int posA = value.indexOf(a);
        if (posA == -1) {
            return "";
        }
        return value.substring(0, posA);
    }

    static String after(String value, String a) {
        // Returns a substring containing all characters after a string.
        int posA = value.lastIndexOf(a);
        if (posA == -1) {
            return "";
        }
        int adjustedPosA = posA + a.length();
        if (adjustedPosA >= value.length()) {
            return "";
        }
        return value.substring(adjustedPosA);
    }

    /**
     * Throws an exception if any strings are empty or null.
     *
     * @param e Exception to throw
     * @param strings List of strings
     * @throws Exception
     */
    public static void validate( Class<? extends Exception> e, String...strings) throws Exception {
        if( Arrays.stream( strings ).anyMatch(s -> s == null || s.isEmpty()) ) throw e.newInstance();
    }

    public static boolean isEmpty( String input ){
        if( input == null || input.isEmpty() ) return true;
        return false;
    }

    public static boolean isNullOrEmptyOrWhiteSpace(String input) {
        if( isEmpty( input ) || input.trim().isEmpty() ) return true;
        return false;
    }

    public static String isUUID( Object input , Class<? extends Exception> exception ) throws Exception {
        if( exception == null ) throw new ClassNotFoundException();
        if( input == null ) throw exception.getConstructor().newInstance();
        String i = input.toString().trim();
        if( !StringUtils.p.matcher( i ).matches() ) throw exception.getConstructor().newInstance();
        return i;
    }

    public static String isUUID( Object input , String fallback ){
        if( input == null ) return fallback;
        String i = input.toString().trim();
        if( !StringUtils.p.matcher( i ).matches() ) return fallback;
        return i;
    }

    public static boolean isUUID( Object input ){
        if( input == null ) return false;
        String i = input.toString().trim();
        return StringUtils.p.matcher( i ).matches();
    }

    public static int isInt( Object input , int fallback ){
        if( input == null ) return fallback;
        String i = input.toString().trim();
        try{
            return Integer.parseInt( input.toString() );
        }catch (Exception e){
            return fallback;
        }
    }
}
