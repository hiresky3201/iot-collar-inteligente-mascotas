package me.iot.communication.monitor;

import dev.morphia.query.experimental.filters.Filters;
import me.iot.communication.TaskManager;
import me.iot.communication.messages.BatteryMessage;
import me.iot.communication.messages.GPSMessage;
import me.iot.communication.messages.SensorMessage;
import me.iot.communication.messages.WebsocketMessage;
import me.iot.communication.websocket.WebsocketManager;
import me.iot.entities.*;
import me.iot.persistence.Persistence;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BatteryMessageMonitor extends MessageMonitor {
    @Override
    public void process(String topic, SensorMessage message) throws Exception {
        BatteryMessage batteryMessage = ( BatteryMessage ) message;
        Persistence persistence = new Persistence();

        Pet pet = persistence.executeQuery( Pet.class, Filters.eq( "sensorId", batteryMessage.getSensorId() ) ).stream().findFirst().orElse( null );

        if( pet == null ) return;

        User user = persistence.getEntityById( User.class, pet.getUserId() );

        List<Alert> batteryAlerts = persistence.executeQuery(
                Alert.class, Filters.eq("userId", user.getId() ),
                Filters.eq( "sensorId", batteryMessage.getSensorId() ),
                Filters.eq("isActive", true)
        ).stream().filter( a -> a.getType().equals( Alert.Type.BATTERY ) ).collect( Collectors.toList() );

        // -- Submit metrics
        Metric metric = new Metric();
        metric.setRelevantId( pet.getId() );
        metric.setDoubleValue( batteryMessage.getBatteryLevel() );
        metric.setTimestamp( LocalDateTime.now() );
        metric.setType( Metric.Type.BATTERY_LEVEL );

        persistence.createEntity( metric );

        batteryAlerts.forEach( a -> {
            // -- Cool-down between alert notifications
            if( a.getLastAlertNotification() != null && a.getLastAlertNotification().plusMinutes( a.getMinutesBetweenAlertNotifications() ).isAfter( LocalDateTime.now() ) ) return;

            a.setLastAlertNotification( LocalDateTime.now() );
            persistence.merge( a );

            if( Alert.Method.EMAIL.equals( a.getMethod() ) ){
                TaskManager.sendBatteryAlertEmail( user, pet );

                Log log = new Log();
                log.setUserId( user.getId() );
                log.setTimestamp( LocalDateTime.now() );
                log.setDescription( "Envio de alerta de Bateria por medio de email" );

                persistence.createEntity( log );
            }
            else if( Alert.Method.WEBSOCKET.equals( a.getMethod() ) ){
                WebsocketMessage notificationMessage = new WebsocketMessage();
                Map<String, Object> model = new HashMap<>();

                String notificationTitle = "Alerta de bateria del collar";
                String notificationBody = String.format( "Tu mascota %s tiene un nivel de bateria de %s", pet.getName(), batteryMessage.getBatteryLevel() ) + "%";

                model.put("title", notificationTitle);
                model.put( "body", notificationBody );

                notificationMessage.setData( model );
                notificationMessage.setType( WebsocketMessage.Type.PUSH_NOTIFICATION );

                TaskManager.sendWebsocketMessage( user.getId(), notificationMessage );

                Log log = new Log();
                log.setUserId( user.getId() );
                log.setTimestamp( LocalDateTime.now() );
                log.setDescription( "Envio de alerta de Bateria por medio de websocket" );

                persistence.createEntity( log );
            }
        } );

        sendPetDataThroughWebsocket( user.getId(), pet.getId(), batteryMessage.getBatteryLevel() );
    }

    private void sendPetDataThroughWebsocket( String userId, String petId, float batteryLevel ){
        WebsocketMessage websocketMessage = new WebsocketMessage();
        Map<String, Object> model = new HashMap<>();
        model.put( "petId", petId );
        model.put( "batteryLevel", batteryLevel );
        websocketMessage.setData( model );
        websocketMessage.setType( WebsocketMessage.Type.BATTERY );

        WebsocketManager.getInstance().sendMessageTo( userId, websocketMessage );
    }

    @Override
    public String[] topics() {
        return new String[]{ "BATTERY" };
    }
}
