package me.iot.communication.monitor;

import me.iot.communication.messages.SensorMessage;

public abstract class MessageMonitor {
    public abstract String[] topics();
    public abstract void process(String topic, SensorMessage message) throws Exception;
}
