package me.iot.communication.monitor;

import dev.morphia.query.experimental.filters.Filters;
import me.iot.communication.TaskManager;
import me.iot.communication.messages.GPSMessage;
import me.iot.communication.messages.SensorMessage;
import me.iot.communication.messages.WebsocketMessage;
import me.iot.communication.websocket.WebsocketManager;
import me.iot.entities.*;
import me.iot.persistence.Persistence;
import me.iot.utils.GeoUtils;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GpsMessageMonitor extends MessageMonitor {
    @Override
    public void process(String topic, SensorMessage message) throws Exception {
        GPSMessage gpsMessage = ( GPSMessage ) message;
        Persistence persistence = new Persistence();

        Pet pet = persistence.executeQuery( Pet.class, Filters.eq( "sensorId", gpsMessage.getSensorId() ) ).stream().findFirst().orElse( null );

        if( pet == null ) return;

        User user = persistence.getEntityById( User.class, pet.getUserId() );

        List<Alert> gpsAlerts = persistence.executeQuery(
            Alert.class, Filters.eq("userId", user.getId() ),
            Filters.eq( "sensorId", gpsMessage.getSensorId() ),
            Filters.eq("isActive", true)
        ).stream().filter( a -> a.getType().equals( Alert.Type.GPS_MID ) || a.getType().equals( Alert.Type.GPS_MAX ) ).collect( Collectors.toList() );

        Area area = persistence.getEntityById( Area.class, pet.getAreaId() );

        // -- Verifies if the pet is in a good area
        float distanceInMts = GeoUtils.haversine( area.getLat(), area.getLng(), gpsMessage.getLat(), gpsMessage.getLng() );

        // -- Submit metrics
        Metric metric = new Metric();
        metric.setRelevantId( pet.getId() );
        metric.setDoubleValue( distanceInMts );
        metric.setTimestamp( LocalDateTime.now() );
        metric.setType( Metric.Type.DISTANCE_TO_AREA_BORDER );

        persistence.createEntity( metric );

        // -- Send user current distance in Mts for their current pet through the websocket
        sendPetDataThroughWebsocket( pet.getUserId(), pet.getId(), gpsMessage.getLat(), gpsMessage.getLng(), distanceInMts );

        // -- If user has no alerts set, then dont do anything
        if( gpsAlerts.isEmpty() ) return;

        if( distanceInMts >= area.getMaxLevelMeters() ){
            notifyGPSAlert( persistence, user, pet, area, distanceInMts, Alert.Type.GPS_MAX, gpsAlerts );
        }
        else if( distanceInMts >= area.getMidLevelMeters() ){
            notifyGPSAlert( persistence, user, pet, area, distanceInMts, Alert.Type.GPS_MID, gpsAlerts );
        }
    }

    private void notifyGPSAlert( Persistence persistence, User user, Pet pet, Area area, float distanceInMts, Alert.Type alertType, List<Alert> alerts ){
        List<Alert> gpsAlerts = alerts.stream().filter( a -> alertType.equals( a.getType() ) ).collect( Collectors.toList() );
        gpsAlerts.forEach( a -> {
            // -- Cool-down between alert notifications
            if( a.getLastAlertNotification() != null && a.getLastAlertNotification().plusMinutes( a.getMinutesBetweenAlertNotifications() ).isAfter( LocalDateTime.now() ) ) return;

            a.setLastAlertNotification( LocalDateTime.now() );
            persistence.merge( a );

            if( Alert.Method.EMAIL.equals( a.getMethod() ) ){
                TaskManager.sendGpsAlertEmail( user, pet );

                Log log = new Log();
                log.setUserId( user.getId() );
                log.setTimestamp( LocalDateTime.now() );
                log.setDescription( "Envio de alerta de GPS por medio de email" );

                persistence.createEntity( log );
            }
            else if( Alert.Method.WEBSOCKET.equals( a.getMethod() ) ){
                WebsocketMessage notificationMessage = new WebsocketMessage();
                Map<String, Object> model = new HashMap<>();

                String notificationTitle = "";
                String notificationBody = "";

                if( alertType.equals( Alert.Type.GPS_MAX ) ){
                    notificationTitle = "ALERTA MAXIMA DE MASCOTA";
                    notificationBody = String.format( "Tu mascota %s se ha salido de %s por completo, y esta a %s de distancia", pet.getName(), area.getName(), distanceInMts );
                }
                else if ( alertType.equals( Alert.Type.GPS_MID ) ){
                    notificationTitle = "Alerta media de mascota";
                    notificationBody = String.format( "Tu mascota %s se ha pasado el nivel medio de el area %s, y esta a %s de distancia", pet.getName(), area.getName(), distanceInMts );
                }

                model.put("title", notificationTitle);
                model.put( "body", notificationBody );

                notificationMessage.setData( model );
                notificationMessage.setType( WebsocketMessage.Type.PUSH_NOTIFICATION );

                TaskManager.sendWebsocketMessage( user.getId(), notificationMessage );

                Log log = new Log();
                log.setUserId( user.getId() );
                log.setTimestamp( LocalDateTime.now() );
                log.setDescription( "Envio de alerta de GPS por medio de websocket" );

                persistence.createEntity( log );
            }
        } );
    }

    private void sendPetDataThroughWebsocket( String userId, String petId, float lat, float lng, float distanceInMts ){
        WebsocketMessage websocketMessage = new WebsocketMessage();
        Map<String, Object> model = new HashMap<>();
        model.put( "petId", petId );
        model.put( "lat", lat );
        model.put( "lng", lng );
        model.put( "distanceInMts", distanceInMts );
        websocketMessage.setData( model );
        websocketMessage.setType( WebsocketMessage.Type.GPS_UPDATE );

        WebsocketManager.getInstance().sendMessageTo( userId, websocketMessage );
    }

    @Override
    public String[] topics() {
        return new String[]{ "GPS" };
    }
}
