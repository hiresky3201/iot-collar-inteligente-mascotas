package me.iot.communication.monitor;

import me.iot.communication.messages.BatteryMessage;
import me.iot.communication.messages.GPSMessage;
import me.iot.communication.messages.SensorMessage;
import me.iot.utils.JacksonUtil;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MqttMessageManager implements IMqttMessageListener {
    private static final List<MessageMonitor> messageMonitors = new ArrayList<>();

    public MqttMessageManager(){
        messageMonitors.add( new GpsMessageMonitor() );
        messageMonitors.add( new BatteryMessageMonitor() );
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage){
        // -- Parse message depending on the topic
        SensorMessage message = parseMessageForTopic( topic, mqttMessage.getPayload() );

        // -- Only pass message to relevant monitors
        messageMonitors.stream().filter( a -> Arrays.asList( a.topics() ).contains( topic ) ).forEach(a -> {
            try {
                a.process( topic, message );
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private SensorMessage parseMessageForTopic(String topic, byte[] payload){
        String data = new String( payload );

        if( topic.equalsIgnoreCase( "GPS" ) ){
            return JacksonUtil.readValue( data, GPSMessage.class );
        }
        else if( topic.equalsIgnoreCase( "BATTERY" ) ){
            return JacksonUtil.readValue( data, BatteryMessage.class );
        }
        else
            return null;
    }
}
