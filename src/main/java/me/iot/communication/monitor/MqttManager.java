package me.iot.communication.monitor;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.text.SimpleDateFormat;
import java.util.Arrays;

public class MqttManager implements MqttCallback{
    private static MqttClient client;
    private static final int qos = 2;
    private static final MemoryPersistence persistence = new MemoryPersistence();

    public static void init(String broker, String clientId) throws Exception{
        client = new MqttClient( broker, clientId, persistence );
        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setCleanSession(true);

        System.out.printf("Connectando al servicio: %s%n", broker );
        client.setCallback( new MqttManager() );
        client.connect(connOpts);
        System.out.println("Conexion establecida correctamente");
    }

    public static void publish( String messageContent, String...topics ){
        publish( messageContent.getBytes(), topics );
    }

    public static void publish( byte[] messageContent, String...topics ){
        MqttMessage message = new MqttMessage( messageContent );
        message.setQos( qos );

        Arrays.stream( topics ).forEach(topic -> {
            try {
                client.publish( topic, message  );
            } catch (MqttException e) {
                e.printStackTrace();
            }
        });
    }

    public static void subscribe(IMqttMessageListener listener, String...topics){
        Arrays.stream( topics ).forEach( topic -> {
            try {
                client.subscribe( topic, listener );
            } catch (MqttException e) {
                e.printStackTrace();
            }
        });
    }

    public static void subscribe(Class<? extends IMqttMessageListener> klass, String...topics) throws Exception {
        subscribe( klass.getDeclaredConstructor().newInstance(), topics );
    }

    public static MemoryPersistence getPersistence(){ return persistence; }

    @Override
    public void connectionLost(Throwable arg0) {
        arg0.printStackTrace();
        System.out.println("Connection lost at : " + new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss").format(new java.util.Date()));
    }

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {}

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {}
}