package me.iot.communication;

import me.iot.communication.messages.WebsocketMessage;
import me.iot.communication.websocket.WebsocketManager;
import me.iot.entities.Pet;
import me.iot.entities.User;
import me.iot.utils.EmailUtil;
import me.iot.utils.TemplateEngine;
import me.iot.utils.enums.EmailContentType;
import spark.ModelAndView;

import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TaskManager {
    public static TemplateEngine templateEngine = new TemplateEngine();
    private static ExecutorService mailer = Executors.newFixedThreadPool(1);
    private static ExecutorService websocketSender = Executors.newFixedThreadPool(1);

    public static void sendBatteryAlertEmail(User user, Pet pet){
        mailer.submit(() -> {
            try{
                HashMap<String,Object> model = new HashMap<>();
                model.put("user" , user);
                model.put("pet", pet);

                String content = templateEngine.render( new ModelAndView( model , "battery-alert-email.hbs" ));
                EmailUtil.sendEmail( user.getEmail(), "Alerta de mascota", content, EmailContentType.HTML_TEXT , null );
            }catch ( Exception e ){
                e.printStackTrace();
            }
        });
    }

    public static void sendGpsAlertEmail(User user, Pet pet){
        mailer.submit(() -> {
            try{
                HashMap<String,Object> model = new HashMap<>();
                model.put("user" , user);
                model.put("pet", pet);

                String content = templateEngine.render( new ModelAndView( model , "gps-alert-email.hbs" ));
                EmailUtil.sendEmail( user.getEmail(), "Alerta de mascota", content, EmailContentType.HTML_TEXT , null );
            }catch ( Exception e ){
                e.printStackTrace();
            }
        });
    }

    public static void sendWebsocketMessage(String userId, WebsocketMessage message){
        websocketSender.submit(() -> {
            WebsocketManager.getInstance().sendMessageTo( userId, message );
        });
    }
}
