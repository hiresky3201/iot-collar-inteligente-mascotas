package me.iot.communication.messages;

import java.util.UUID;

public class WebsocketMessage {
    private final String id = UUID.randomUUID().toString();
    private Object data;
    private Type type;
    private boolean ensureMessage;

    public enum Type{
        PING,
        GPS_UPDATE,
        SOUND_EMIT_REQUEST,
        PUSH_NOTIFICATION,
        BATTERY
    }

    public WebsocketMessage(){}
    public WebsocketMessage( String data, boolean ensureMessage ){
        this.data = data;
        this.ensureMessage = ensureMessage;
    }

    public Type getType() { return type; }

    public void setType(Type type) { this.type = type; }

    public String getId() { return id; }

    public Object getData() {
        return data;
    }

    public void setData(Object data) { this.data = data; }

    public boolean isEnsureMessage() { return ensureMessage; }

    public void setEnsureMessage(boolean ensureMessage) { this.ensureMessage = ensureMessage; }
}
