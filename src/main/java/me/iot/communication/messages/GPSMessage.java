package me.iot.communication.messages;

public class GPSMessage implements SensorMessage{
    private String sensorId;
    private float lat;
    private float lng;

    public GPSMessage(){}

    public String getSensorId() { return sensorId; }

    public void setSensorId(String sensorId) { this.sensorId = sensorId; }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }
}
