package me.iot.communication.messages;

public class BatteryMessage implements SensorMessage{
    private String sensorId;
    private float batteryLevel;

    public String getSensorId() { return sensorId; }

    public void setSensorId(String sensorId) { this.sensorId = sensorId; }

    public float getBatteryLevel() { return batteryLevel; }

    public void setBatteryLevel(float batteryLevel) { this.batteryLevel = batteryLevel; }
}
