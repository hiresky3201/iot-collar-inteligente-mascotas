package me.iot.communication.websocket;

import me.iot.communication.messages.WebsocketMessage;
import me.iot.communication.monitor.MqttManager;
import me.iot.utils.HttpUtil;
import me.iot.utils.JacksonUtil;
import org.apache.commons.lang3.tuple.Pair;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class WebsocketManager extends WebSocketServer {
    private static WebsocketManager instance;
    private final Map<String, WebSocket> connections = new HashMap<>();
    private final ConcurrentHashMap<String, Pair<WebsocketMessage, LocalDateTime>> pendingMessages = new ConcurrentHashMap<>();

    protected WebsocketManager(){}
    protected WebsocketManager(InetSocketAddress address){ super( address ); }

    public static void init( String host, int port ){
        WebsocketManager.instance = new WebsocketManager( new InetSocketAddress( host, port ) );
        WebsocketManager.instance.start();
    }

    public void sendMessageTo( String userId, WebsocketMessage message ){
        //System.out.println("MESSAGE SENT: " + message.toString());

        if( !connections.containsKey( userId ) ) return;

        // -- Si el mensaje es MUY importante, agregarlo a un batch que se
        // encargara de verificar si el cliente mando un ACKNOWLEDGE
        //
        // Si este no se recibe, entonces se volver a mandar el mensaje
        if( message.isEnsureMessage() ){
            ensureMessage( userId, message );
        }

        connections.get( userId ).send( JacksonUtil.writeValueAsString( message ) );
    }

    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        Map<String, String> queryMap = HttpUtil.queryToMap( clientHandshake.getResourceDescriptor() );

        if( !queryMap.containsKey( "userId" ) ){
            webSocket.send("Es necesario conectarte usando un userId como parametro. EJ: ws://endpoint?userId=ID");
            webSocket.close();
            return;
        }

        String userId = queryMap.get( "userId" );
        System.out.printf("Cliente conectado - %s -> %s%n", getAddress(), userId );
        webSocket.setAttachment( userId );
        connections.put( userId, webSocket );
    }

    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        if( webSocket.getAttachment() == null ) return;

        String uuid = webSocket.getAttachment().toString();
        System.out.printf("Cliente desconectado - %s -> %s%n", getAddress(), uuid );
        connections.remove( uuid );
    }

    public void onMessage(WebSocket webSocket, String s) {
        String uuid = webSocket.getAttachment().toString();
        System.out.printf( "[%s - %s]: %s%n", getAddress(), uuid, s );

        WebsocketMessage message = JacksonUtil.readValue( s, WebsocketMessage.class );

        if( WebsocketMessage.Type.SOUND_EMIT_REQUEST.equals( message.getType() ) ){
            MqttManager.publish( JacksonUtil.writeValueAsString( message ), message.getData() + "/SOUND" );
        }
        if( WebsocketMessage.Type.PING.equals( message.getType() ) ){
            connections.values().forEach( c -> c.send( s ) );
        }

        Acknowledge acknowledge = JacksonUtil.tryReadValue( s, Acknowledge.class );

        if( acknowledge != null ) { acknowledge( acknowledge ); }
    }

    public void onError(WebSocket webSocket, Exception e) {
        System.err.printf("Error de conexion - %s%n", getAddress() );
        e.printStackTrace();
    }

    @Override
    public void onStart() {
        // -- Crear un scheduler que se encarga de verificar si han llegado mensajes
        ScheduledExecutorService messageMonitor = Executors.newScheduledThreadPool(1);
        messageMonitor.scheduleAtFixedRate( this::verifyPendingMessages, 0, 30, TimeUnit.SECONDS );

        System.out.printf("Websocket Server inicializado - %s%n", getAddress() );
    }

    private void verifyPendingMessages(){
        pendingMessages.forEach((key, p) -> {
            // -- Si ya paso 30 segundos despues de que se mando un mensaje, volver a mandarlo
            if ( p.getRight().isAfter( LocalDateTime.now() ) ) return;

            // -- Mandar el mensaje, y volver a poner un timestamp para volver a verificar si se necesita mandare le mensaje otra vez
            this.connections.get( key ).send( JacksonUtil.writeValueAsString( p.getLeft() ) );
            pendingMessages.replace( p.getLeft().getId(), Pair.of( p.getLeft(), LocalDateTime.now().plus( 30, ChronoUnit.SECONDS ) ) );
        });
    }

    private void acknowledge(Acknowledge acknowledge){
        this.pendingMessages.forEach((key, p) -> {
            if( !p.getLeft().getId().equalsIgnoreCase( acknowledge.getMessageId() ) ) return;
            this.pendingMessages.remove( key );
        });
    }

    private void ensureMessage(String userId, WebsocketMessage message){
        pendingMessages.put( userId, Pair.of( message, LocalDateTime.now().plus( 30, ChronoUnit.SECONDS ) ) );
    }

    public static WebsocketManager getInstance(){ return instance; }
}

class Acknowledge {
    private String messageId;

    public String getMessageId() { return messageId; }

    public void setMessageId(String messageId) { this.messageId = messageId; }
}