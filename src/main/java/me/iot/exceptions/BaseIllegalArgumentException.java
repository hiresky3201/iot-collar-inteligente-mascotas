package me.iot.exceptions;

public class BaseIllegalArgumentException extends IllegalArgumentException {
    public BaseIllegalArgumentException(String s) {
        super(s);
    }
}