package me.iot.exceptions;

public class MalformedJsonException extends BaseIllegalArgumentException {
    public MalformedJsonException(String s) {
        super(s);
    }
    public MalformedJsonException() {
        super("Invalid JSON format");
    }
}