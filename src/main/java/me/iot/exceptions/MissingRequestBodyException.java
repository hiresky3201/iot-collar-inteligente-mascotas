package me.iot.exceptions;

public class MissingRequestBodyException extends BaseIllegalArgumentException {
    public MissingRequestBodyException(String s) {
        super(s);
    }
    public MissingRequestBodyException() {
        super("Required body is missing");
    }
}
