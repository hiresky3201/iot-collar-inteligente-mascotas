package me.iot.exceptions;

public class InvalidUserOrPasswordException extends BaseIllegalArgumentException {
    public InvalidUserOrPasswordException(String s) {
        super(s);
    }
    public InvalidUserOrPasswordException() {
        super("Tu usuario o contrasena son incorrectas");
    }
}