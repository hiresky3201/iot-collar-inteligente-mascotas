package me.iot.exceptions;

public class InvalidEntityException extends BaseIllegalArgumentException {
    public InvalidEntityException(String s) {
        super(s);
    }
    public InvalidEntityException() {
        super("Invalid Entity ID");
    }
}