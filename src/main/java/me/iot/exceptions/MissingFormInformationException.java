package me.iot.exceptions;

public class MissingFormInformationException extends BaseIllegalArgumentException {
    public MissingFormInformationException(String s) {
        super(s);
    }
    public MissingFormInformationException() {
        super("One or more required fields are missing");
    }
}