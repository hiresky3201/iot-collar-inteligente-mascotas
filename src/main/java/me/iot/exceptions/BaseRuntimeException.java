package me.iot.exceptions;

public class BaseRuntimeException extends RuntimeException {
    public BaseRuntimeException(String s) {
        super(s);
    }
    public BaseRuntimeException(String s,Exception e) {
        super(s,e);
    }
    public BaseRuntimeException(Throwable s) {
        super(s);
    }
    public BaseRuntimeException(Exception s) {
        super(s);
    }
}