package me.iot.exceptions;

public class ExistingSensorIdException extends BaseIllegalArgumentException {
    public ExistingSensorIdException(String s) {
        super(s);
    }
    public ExistingSensorIdException() {
        super("Ya hay una mascota con este ID de sensor");
    }
}