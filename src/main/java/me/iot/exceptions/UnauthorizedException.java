package me.iot.exceptions;

public class UnauthorizedException extends BaseRuntimeException {
    public UnauthorizedException(String s) {
        super(s);
    }
    public UnauthorizedException() {
        super("You are not authorized to access this route");
    }
}